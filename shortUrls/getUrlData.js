const { firestore } = require("../connections/firestore");

//TODO: add method to varify owner token

const getUrlData = async({shortUrl}) =>{
    console.log("getting url data")
    try{
        let querySnapshot = await firestore.collection('short_urls').where("shortUrl", "==", shortUrl).orderBy('createdAt').limit(1).get();
        let urlData = querySnapshot?.docs?.[0]?.data() || false
        if (urlData) {
            urlData = {...urlData, docId: querySnapshot?.docs?.[0]?.id}
        }
        return urlData
    }catch(err){
        console.log("error in getting url docs from firestore", err)
        return false
    }
}

module.exports = {getUrlData: getUrlData}