const { firestore, FieldValue } = require("../connections/firestore");

//TODO: add method to varify owner token
//TODO: support owner uid

const incrementUrlCounter = async({shortUrl}) =>{
    const { getUrlData } = require("./getUrlData")

    console.log(shortUrl)
    const data = {
        counter: FieldValue.increment(1),
    };
    try{
        let urlData = await getUrlData({shortUrl})
        let shortUrlEntryId = urlData?.docId
        if (!shortUrlEntryId) {throw new Error("couldnt get desired url")}
        await firestore.collection('short_urls').doc(shortUrlEntryId).set(data, { merge: true });
        return true
    }catch(err){
        console.log("error in writing new short url to firestore", err)
        return false
    }
}

module.exports = {incrementUrlCounter: incrementUrlCounter}