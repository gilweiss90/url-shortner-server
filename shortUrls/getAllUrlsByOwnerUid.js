const { firestore, FieldValue } = require("../connections/firestore");

//TODO: add method to varify owner token
//TODO: support owner uid
const URL_DOC_MAX_LIMIT = 500
const getAllUrlsByOwnerUid = async({ownerUid}) =>{
    try{
        querySnapshot = await firestore.collection('short_urls').where("ownerUid", "==", "placeHolder").orderBy('createdAt').limit(URL_DOC_MAX_LIMIT).get();
        let allUrlsDataByOwnerUid = []
        querySnapshot?.docs.forEach(doc => { 
            allUrlsDataByOwnerUid.push(doc.data())
        });
        return allUrlsDataByOwnerUid
    }catch(err){
        console.log("error in getting url docs from firestore", err)
        return false
    }
}

module.exports = {getAllUrlsByOwnerUid: getAllUrlsByOwnerUid}