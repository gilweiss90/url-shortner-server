const { firestore } = require("../connections/firestore");

//TODO: add method to varify owner token
const deleteShortUrl = async({urlToDelete}) =>{
    console.log(urlToDelete)
    try{
        await firestore.runTransaction(async (transaction) => {
            const urlDocs = await transaction.get(firestore.collection('short_urls').where("shortUrl", '==', urlToDelete));
            let urlDocId = urlDocs?.docs?.[0]?.id
            if (urlDocId){
                transaction.delete(firestore.collection('short_urls').doc(urlDocId));
            } else {
                throw new Error ("couldnt find doc to delete")
            }
        })
    }catch(err){
        console.log("deleteShortUrl failed", err)
        return false
    }
    return true
}

module.exports = {deleteShortUrl: deleteShortUrl}