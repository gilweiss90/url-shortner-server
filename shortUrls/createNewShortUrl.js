const { firestore, FieldValue } = require("../connections/firestore");

//TODO: add method to varify owner token
//TODO: support owner uid

const createNewShortUrl = async({shortUrl, longUrl}) =>{
    console.log(shortUrl)
    const data = {
        shortUrl: shortUrl,
        longUrl: longUrl,
        createdAt: FieldValue.serverTimestamp(),
        counter: 0,
        ownerUid: "placeHolder",
        domain: "plc.hd" //TODO : get this resource from somewhere else if we want to have more than 1 domain
    };
    try{
        await firestore.collection('short_urls').add(data);
        return true
    }catch(err){
        console.log("error in writing new short url to firestore", err)
        return false
    }
}

module.exports = {createNewShortUrl: createNewShortUrl}