const { postgressPool } = require("../connections/postgress");
const base62 = require("base62/lib/ascii");


const populateIdTicket = async ({ticketId, encodedTicket}) => {
    let ticketIdToUse = encodedTicket 
        ?   base62.decode(ticketId)
        :   ticketId

    console.log("ticketIdToUse", ticketId, ticketIdToUse)

    let populateIdTicketQueryText = (
        'UPDATE id_ticket_table'+
        '    SET populated = true '+
        'WHERE id = $1  ' /* ticketId */+
        '    AND populated = false  '+
        'RETURNING *;'
    )
    let populateIdTicketQuery = {
        text: populateIdTicketQueryText,
        values: [ticketIdToUse]
    }
    try{
        queryResult = await postgressPool.query(populateIdTicketQuery)
        if (queryResult?.command === "UPDATE" && queryResult?.rowCount > 0){
            return true
        }
    }catch(err){
        console.log("couldnt populate given ticket" , err)
        return false
    }
    return false
}

    
module.exports = {populateIdTicket: populateIdTicket}



