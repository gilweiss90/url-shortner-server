const { postgressPool } = require("../connections/postgress");
const  base62  = require("base62/lib/ascii");

const getIdTicket = async ({encodeTicket}) => {
    console.log("getting new id ticket")
    let getIdTicketQueryText = (
        'INSERT INTO id_ticket_table (populated) VALUES (false) '+
        'RETURNING *;'
    )
    try{
        queryResult = await postgressPool.query(getIdTicketQueryText)
        if (queryResult?.command === "INSERT" && queryResult?.rowCount > 0){
            let idTicket = queryResult?.rows?.[0]?.["id"]
            idTicket = idTicket >=0 && encodeTicket
                ?   base62.encode(idTicket)
                :   false
            return idTicket
        }
        return false
    }catch(err){
        console.log("failed to get new id ticket", err)
        return false
    }
}

module.exports = {getIdTicket: getIdTicket}