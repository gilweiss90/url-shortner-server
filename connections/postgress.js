require("dotenv").config();
const { startupPostgress } = require("./postgress/startupPostgress");
const { Pool } = require('pg');

const postgressPool = new Pool({
    connectionString: process.env.POSTGRES_URL,
    ssl: false
});

(async()=>await startupPostgress({pool: postgressPool}))()





module.exports = { postgressPool };

