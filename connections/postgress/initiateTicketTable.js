
const { tableExists } = require("./helperFunctions")


const initiateTicketTable = async ({pool}) => {

    const CREATE_ID_TICKET_TABLE_QUERY =
    'CREATE TABLE id_ticket_table (' +
    '    id SERIAL PRIMARY KEY,' +
    '    populated BOOLEAN DEFAULT false);'+
    '    SELECT setval(\'id_ticket_table_id_seq\', 10000)'; //started at 10000 because didnt want to give very short urls
  
    createIdTicketTable = (pool) => {
        console.log("creating id ticket table in postgress");
        return new Promise(function (resolve, reject) {
            pool.query(
                CREATE_ID_TICKET_TABLE_QUERY
                , async (err, res) => {
                    if (err) {
                        console.log("err: ", err)
                        reject(new Error('Ooops, something broke! in createIdTicketTable func', err));
                    } else {
                        console.log("id ticket table was created succesfully!");
                        resolve("success");
                    }
                }
            );
        });
    };
    
    startInitiateTicketTable = async (pool) => {
        try{
            if (await tableExists(pool, 'id_ticket_table')){
                console.log("postgress id ticket table exists and running");
                return ("success")
            }else{
                console.log("try create")
                let createIdTicketTableRes = await createIdTicketTable(pool);
                if (createIdTicketTableRes === "success"){
                    console.log("postgress id ticket table was created and now running");
                    return ("success")
                }else{
                    console.log("postgress failed to initiate id ticket table!1");
                    return ("error")
                }
            }
        }catch(err) {
            console.log("postgress failed to initiate id ticket table!2", err);
            return ("error");
        }
    }

    initiateTicketTableRes = await startInitiateTicketTable(pool)
    return (initiateTicketTableRes === "success")
}


module.exports = {initiateTicketTable: initiateTicketTable}
