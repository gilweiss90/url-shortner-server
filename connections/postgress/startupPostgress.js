

const startupPostgress = async ({pool}) => {

    console.log("postgress startup")
    const initiationProcedures = []
    let initiationStatus = true

    //add initiation functions
    const { initiateTicketTable } = require("./initiateTicketTable");
    initiationProcedures.push(async ()=>{return await initiateTicketTable({pool: pool})})

    //activate initiation functions
    for (const initiationProcedure of initiationProcedures) {
        let initiationProceduresStatus = await initiationProcedure()
        if (! initiationProceduresStatus) {initiationStatus = false}
    }

    //status notification
    initiationStatus 
        ?   console.log("postgress initiated succesfully")
        :   console.log("error in postgress initiation!")

    return initiationStatus
}

module.exports = {startupPostgress: startupPostgress}
