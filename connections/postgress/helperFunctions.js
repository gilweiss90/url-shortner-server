const tableExists = (pool, tableName) => {
    return new Promise(function (resolve, reject) {
        pool.query(
            'SELECT EXISTS' +
            '(' +
            'SELECT 1' +
            'FROM information_schema.tables ' +
            'WHERE table_schema = \'public\'' +
            'AND table_name = \'' + tableName + '\'' +
            ');'
            , async (err, res) => {
                if (err) {
                    reject(new Error('Ooops, something went wrong in tableExists func!'));
                }else {
                    var results = { 'results': (res) ? res.rows : null };
                    results = JSON.stringify(results).includes("true");
                    resolve(results);
                }
            }
        );
    });
};

module.exports = {tableExists: tableExists}