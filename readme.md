
# deploying:

1)set up a postgress db, save connection string in dotenv

2)set up firestore project, save connection json in '../secrets/firestoreConfig.json

3)deploy server

4)set client's './src/configs/serverApi to contain server's url for API requests

5)deploy client

6)check out server's console logs - in first use we will require to create firestore indexes, which are produced automaticly after clicking the two generated links (easiest way to do so)


## notes

i've made -numerous- cutbacks in this project as a real url shortner deserves much more caring to details and scale, but the main flow and ux are demonstrated

## client repo

https://gitlab.com/gilweiss90/url-shortner

## server repo

https://gitlab.com/gilweiss90/url-shortner-server