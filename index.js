require("dotenv").config();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('dist'));
require("./connections/postgress");
require("./connections/firestore");


///////////////////////////////////////API/////////////////////////////////////////

    //for local development, if id spend more time i would load this code only on dev mode
    app.use(require('cors')());

    app.get(
        '/api/urls/lease/getNewShortUrl',
        async (request, response) => {
            //TODO: ask for ownerUid
            //TODO: ask for ownerToken
            const { getIdTicket } = require("./idTickets/getIdTicket")
            idTicket = await getIdTicket({encodeTicket: true}).catch(e => { console.log(e) })
            response.send({idTicket, domain:"plc.hd"}); //TODO: get domain resource from someplace so we could change it in future
        });
  
    app.get(
        '/api/urls',
        async (request, response) => {
            //TODO: ask for ownerUid
            //TODO: ask for ownerToken
            const { getAllUrlsByOwnerUid } = require("./shortUrls/getAllUrlsByOwnerUid")
            let allUrlsDataByOwnerUid = await getAllUrlsByOwnerUid({}).catch(e => { console.log(e) })
            response.send({allUrlsDataByOwnerUid});
        });    

    app.get(
        '/api/urls/:shortUrl',
        async (request, response) => {
            let shortUrl = request?.params?.shortUrl;
            const { getUrlData } = require("./shortUrls/getUrlData")
            let urlData = await getUrlData({shortUrl:shortUrl}).catch(e => { console.log(e) })
            if (urlData){
                response.send({...urlData});
            } else ( response.send(false))
        });    
      
    app.post(
        '/api/urls',
        async (request, response) => {
            let res = false
            let shortUrl = request?.body?.shortUrl;
            let longUrl = request?.body?.longUrl;
            //TODO: ask for ownerUid
            //TODO: ask for ownerToken
            //TODO: add method to varify good values of shortUrl, longUrl
            const { populateIdTicket } = require("./idTickets/populateIdTicket")
            let populated = await populateIdTicket({ticketId:shortUrl, encodedTicket:true}).catch(e => { console.log(e) })
            if (populated){
                const { createNewShortUrl } = require("./shortUrls/createNewShortUrl")
                createNewShortUrl({shortUrl:shortUrl, longUrl:longUrl})
                res = true
            }
            response.send(res);
        }
    );

    app.post(
        '/api/urls/increment/:shortUrl',
        async (request, response) => {
            let shortUrl = request?.params?.shortUrl;
            //TODO: add method to varify good values of shortUrl
            const { incrementUrlCounter } = require("./shortUrls/incrementUrlCounter")
            let res = await incrementUrlCounter({shortUrl:shortUrl}).catch(e => { console.log(e) })
            response.send(res);
        }
    );

    
    app.delete(
        '/api/urls/:urlToDelete',
        async (request, response) => {
            let res = false
            //TODO: ask for ownerUid
            //TODO: ask for ownerToken
            let urlToDelete = request?.params?.urlToDelete;
            const { deleteShortUrl } = require("./shortUrls/deleteShortUrl")
            res = await deleteShortUrl({urlToDelete:urlToDelete})
            response.send(res);
        }
    );
  


app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));


